import { Persona } from './persona'; 
import { Expositor } from './../interfaces/expositor'; 


export class Alumno extends Persona implements Expositor {
	public carrera: string;
	public anio: number = 1; 
	public egresado: boolean = false;

	constructor(nombre: string, carrera: string) {
		super();
		this.nombre = nombre; 
		this.carrera = carrera; 
	}

	public exponer(tema: string, horas: number) {
		console.log(`Exponiendo el tema ${tema} en ${horas} hora(s)`)
	}
}
