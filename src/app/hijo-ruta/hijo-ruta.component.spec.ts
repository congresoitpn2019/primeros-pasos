import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { HijoRutaComponent } from './hijo-ruta.component';

describe('HijoRutaComponent', () => {
  let component: HijoRutaComponent;
  let fixture: ComponentFixture<HijoRutaComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ HijoRutaComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(HijoRutaComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
