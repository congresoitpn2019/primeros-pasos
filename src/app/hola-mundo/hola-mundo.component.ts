import { Component, OnInit } from '@angular/core'; 
import { Alumno } from '../modelos/alumno'; 


@Component({
  selector: 'app-hola-mundo',
  templateUrl: './hola-mundo.component.html',
  styleUrls: ['./hola-mundo.component.css']
})
export class HolaMundoComponent implements OnInit {
  public mensaje = 'Hola mundo desde mi primer componente'; 
  public lista: Array<string> = [ 'Hola', 'Mundo', 'X', 'Y', 'Z' ]; 
  public memeVisible = false; 

  constructor() { } 

  ngOnInit() { 

    var a: any = 23;
    console.log(`Valor de a: ${a}`); 
    a = 'algo'; 
    console.log(`Valor de a: ${a}`); 


    var alumno: any = new Alumno('Gerardo', 'Informática'); 
    console.log(`El alumno ${alumno.nombre} de la carrera ${ alumno.carrera }`); 
    alumno.exponer('Biología', 2);
  } 

  public mostrarMeme() { 
    this.memeVisible = !this.memeVisible; 
    console.log(`Es meme visible ${this.memeVisible}`); 
  } 

  public manejarSalida(texto: string) { 
    this.mensaje = texto; 
  } 

}
