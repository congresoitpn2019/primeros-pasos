import { Component } from '@angular/core'; 
import { Router } from '@angular/router'; 


@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  title = 'primeros-pasos';

  constructor(public router: Router) { }

  public irPorCodigo() {
  	this.router.navigate(['mi-otro-componente', 777]);
  }
} 
