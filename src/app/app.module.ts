import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppComponent } from './app.component';

import { RouterModule, Routes } from '@angular/router';
import { OtroComponent } from './otro/otro.component';
import { HolaMundoComponent } from './hola-mundo/hola-mundo.component';
import { HijoComponent } from './hijo/hijo.component';
import { HijoRutaComponent } from './hijo-ruta/hijo-ruta.component';


const appRoutes: Routes = [ 
  { 
    path: '', 
    redirectTo: 'hola-mundo', 
    pathMatch: 'full' 
  }, 
  { 
    path: 'hola-mundo', 
    component: HolaMundoComponent 
  }, 
  { 
    path: 'mi-otro-componente', 
    component: OtroComponent 
  }, 
  { 
    path: 'mi-otro-componente/:mivalor', 
    pathMatch: 'prefix', 
    children: [
      {
        path: 'hijo-ruta', 
        component: HijoRutaComponent 
      }, 
      {
        path: '', component: OtroComponent, pathMatch: 'full'
      } 
    ] 
  }
] 


@NgModule({
  declarations: [
    AppComponent,
    OtroComponent,
    HolaMundoComponent,
    HijoComponent,
    HijoRutaComponent
  ],
  imports: [
    BrowserModule, 
    RouterModule.forRoot(appRoutes, { enableTracing: false })
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
