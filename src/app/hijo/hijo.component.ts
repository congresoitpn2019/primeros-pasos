import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';

@Component({
  selector: 'app-hijo',
  templateUrl: './hijo.component.html',
  styleUrls: ['./hijo.component.css']
})
export class HijoComponent implements OnInit { 
  @Input('entrada') miEntrada: string = 'valor por defecto'; 
  @Output('salida') miSalida = new EventEmitter<any>(); 

  constructor() { } 

  ngOnInit() { 
  } 

} 
